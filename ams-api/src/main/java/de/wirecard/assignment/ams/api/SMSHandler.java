package de.wirecard.assignment.ams.api;

import de.wirecard.assignment.ams.edge.validation.SmsMessage;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by a.tanoshkin on 10.04.2017.
 */
public interface SMSHandler {

    /**
     * @param smsContent     the incoming SMS command string.
     * @param senderDeviceId is a unique string that uniquely identifies the
     *                       customer’s mobile device. The UserManager proves a means to identify the sender
     *                       user.
     * @return The SMS content that should be returned to the user.
     */
    String handleSmsRequest(@SmsMessage String smsContent, String senderDeviceId);
}
