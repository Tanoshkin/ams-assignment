package de.wirecard.assignment.ams.api;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by a.tanoshkin on 12.04.2017.
 */
public interface TransferManager {

    void sendMoney(String senderUsername, String recipientUsername, BigDecimal
            amount);

    List<BigDecimal> getAllTransactions(String senderUsername, String
            recipientUsername);

}
