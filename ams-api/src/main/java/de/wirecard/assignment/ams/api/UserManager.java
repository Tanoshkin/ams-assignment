package de.wirecard.assignment.ams.api;

import java.math.BigDecimal;

/**
 * Created by a.tanoshkin on 12.04.2017.
 */
public interface UserManager {
    boolean existsUser(String username);
    BigDecimal getBalance(String username);
    String getUserNameForDeviceId(String deviceId);
}
