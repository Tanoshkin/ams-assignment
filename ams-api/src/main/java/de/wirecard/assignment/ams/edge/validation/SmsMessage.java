package de.wirecard.assignment.ams.edge.validation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import java.lang.annotation.Target;

/**
 * Created by a.tanoshkin on 10.04.2017.
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
@Constraint(validatedBy = SmsMessageValidator.class)
@Documented
public @interface SmsMessage {

    String message() default "{de.wirecard.assignment.ams.constraints.sms}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


}
