package de.wirecard.assignment.ams.edge.validation;

import de.wirecard.assignment.ams.model.SmsMessageType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.StringJoiner;
import java.util.regex.Pattern;

/**
 * Created by a.tanoshkin on 10.04.2017.
 */
public class SmsMessageValidator implements ConstraintValidator<SmsMessage, String>  {

    Pattern p;

    @Override
    public void initialize(SmsMessage constraintAnnotation) {
        StringBuilder sb = new StringBuilder();
        StringJoiner sj = new StringJoiner("|", "(", ")");
        for (SmsMessageType messageType : SmsMessageType.values()) {
            if (messageType != SmsMessageType.UNKNOWN) {
                sj.add(messageType.messageType());
            }
        }
        String regexpAsString = sj.toString();
        p = Pattern.compile(regexpAsString);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        // TODO: добавить сюда регулярные выражения для парсинга команд.
        boolean isValid = p.matcher(value).matches();
        if(!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate( "{de.wirecard.assignment.ams.constraints.sms.message}"  ).addConstraintViolation();
        }
        return isValid;
    }

}
