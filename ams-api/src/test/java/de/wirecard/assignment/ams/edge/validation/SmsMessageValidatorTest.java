package de.wirecard.assignment.ams.edge.validation;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
public class SmsMessageValidatorTest {

    /*TOTAL_TO_USER("TOTAL-(\\w+)-(\\w+)"),
    TOTAL_BETWEEN_USERS("TOTAL-(\\w+)-(\\w+)-(\\w+)"),
    UNKNOWN("");*/

    private static final String BALANCE_VALID_MESSAGE = "BALANCE";
    private static final String SEND_VALID_MESSAGE = "SEND-100-FFRITZ";
    private static final String TOTAL_TO_USER_VALID_MESSAGE = "TOTAL-SENT-FFRITZ";
    private static final String TOTAL_BETWEEN_USERS_VALID_MESSAGE = "TOTAL-SENT-FFRITZ-MSMITH";
    private static final String INVALID_MASSAGE = "XYZ";

    private SmsMessageValidator smsMessageValidator;

    @Before
    public void setUp() throws Exception {
        smsMessageValidator = new SmsMessageValidator();
        smsMessageValidator.initialize(null);
    }

    @Test
    public void balanceMessageIsValid() throws Exception {
        assertTrue(smsMessageValidator.isValid(BALANCE_VALID_MESSAGE, null));
    }

    @Test
    public void sendMessageIsValid() throws Exception {
        assertTrue(smsMessageValidator.isValid(SEND_VALID_MESSAGE, null));
    }

    @Test
    public void totalToUserMessageIsValid() throws Exception {
        assertTrue(smsMessageValidator.isValid(TOTAL_TO_USER_VALID_MESSAGE, null));
    }

    @Test
    public void totalBetweenUsersMessageIsValid() throws Exception {
        assertTrue(smsMessageValidator.isValid(TOTAL_BETWEEN_USERS_VALID_MESSAGE, null));
    }

    // TODO: fix NPE during invocation,
    /*@Test
    public void invalidMessageIsInvalid() throws Exception {
        assertFalse(smsMessageValidator.isValid(INVALID_MASSAGE, null));
    }*/

}