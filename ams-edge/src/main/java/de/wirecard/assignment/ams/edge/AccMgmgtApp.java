package de.wirecard.assignment.ams.edge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by a.tanoshkin on 10.04.2017.
 */
@SpringBootApplication
public class AccMgmgtApp {

    public static void main(String[] args) {
        SpringApplication.run(AccMgmgtApp.class, args);
    }

}
