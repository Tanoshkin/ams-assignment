package de.wirecard.assignment.ams.edge.config;

import de.wirecard.assignment.ams.model.Queues;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by a.tanoshkin on 13.04.2017.
 */
@Configuration
public class MessagingConfig {

    @Bean
    public MessageConverter jsonMessageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(Queues.AMS_EXCHANGE.name());
    }

    @Bean
    Queue balanceQueue() {
        return new Queue(Queues.BALANCE_QUEUE.name(), false);
    }

    @Bean
    Queue sendQueue() {
        return new Queue(Queues.SEND_QUEUE.name(), false);
    }

    @Bean
    Queue trnHistQueue() {
        return new Queue(Queues.TRN_HISTORY_QUEUE.name(), false);
    }

    @Bean
    Binding balanceBinding() {
        return BindingBuilder.bind(balanceQueue()).to(exchange()).with(Queues.BALANCE_QUEUE.name());
    }

    @Bean
    Binding sendBinding() {
        return BindingBuilder.bind(sendQueue()).to(exchange()).with(Queues.SEND_QUEUE.name());
    }

    @Bean
    Binding trnHistBinding() {
        return BindingBuilder.bind(trnHistQueue()).to(exchange()).with(Queues.TRN_HISTORY_QUEUE.name());
    }

}
