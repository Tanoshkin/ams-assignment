package de.wirecard.assignment.ams.edge.controller;

import de.wirecard.assignment.ams.api.SMSHandler;
import de.wirecard.assignment.ams.edge.processing.MessageProcessingService;
import de.wirecard.assignment.ams.edge.validation.SmsMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by a.tanoshkin on 10.04.2017.
 * This config is added to make possible usage of our service as RPC call.
 * We could switch the implementation to embeddable library (eg Gradle module) if it's needed.
 */
@RestController
@RequestMapping(path = "/acc-mgmt")
@Validated
@Slf4j
public class AccMgmgtRestController implements SMSHandler {

    private final MessageProcessingService processingService;

    @Autowired
    public AccMgmgtRestController(MessageProcessingService processingService) {
        this.processingService = processingService;
    }

    @Override
    @RequestMapping(value = "/sms-request", method = RequestMethod.POST)
    public String handleSmsRequest(@SmsMessage @RequestParam String smsContent, @RequestParam String senderDeviceId) {
        log.info("Received new SMS request: {} from {}.", smsContent, senderDeviceId);
        String processingResult = processingService.processIncomingMessage(smsContent, senderDeviceId);
        log.info("Returing processing result for {} for {} ", processingResult, senderDeviceId);
        return processingResult;
    }

    @RequestMapping(value = "validation", method = RequestMethod.POST)
    public String validate(@SmsMessage @RequestParam("email") String email) {
        return null;
    }



}
