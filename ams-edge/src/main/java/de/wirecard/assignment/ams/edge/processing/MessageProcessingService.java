package de.wirecard.assignment.ams.edge.processing;

/**
 * Created by a.tanoshkin on 10.04.2017.
 * This interface is needed here for two main reasons:
 * <ul>
 * <li>Here we could work with domain model abstracted from concrete public API visible to clients </li>
 * <li>This service could be implemented as mock for testing purposes - on yearly stages of project it could
 * be useful to give our API to client ASAP,</li>
 * </ul>
 *
 * @author Alexander Tanoshkin
 * @see #processIncomingMessage(String smsContent, String senderDeviceId)
 */

public interface MessageProcessingService {
    /**
     * @param smsContent     the incoming SMS command string.
     * @param senderDeviceId is a unique string that uniquely identifies the
     *                       customer’s mobile device. The UserManager proves a means to identify the sender
     *                       user.
     * @return The SMS content that should be returned to the user.
     */
    public String processIncomingMessage(String smsContent, String senderDeviceId);
}
