package de.wirecard.assignment.ams.edge.processing;

import de.wirecard.assignment.ams.edge.processing.exception.AmountFormatExeption;
import de.wirecard.assignment.ams.edge.processing.exception.MessageFormatException;
import de.wirecard.assignment.ams.model.Queues;
import de.wirecard.assignment.ams.model.SmsMessageType;
import de.wirecard.assignment.ams.model.events.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by a.tanoshkin on 10.04.2017.
 */
@Service
@Slf4j
public class MessageProcessingServiceDefaultImpl implements MessageProcessingService {

    public static final String ERROR_WRONG_MESSAGE_FORMAT = "ERR - WRONG MESSAGE FORMAT.";
    public static final String ERR_INSUFFICIENT_FUNDS = "ERR – INSUFFICIENT FUNDS";
    public static final String ERR_NO_USER = "ERR – NO USER";
    public static final String PLEASE_TRY_AGAIN_LATER = "PLEASE_TRY_AGAIN_LATER";
    public static final String ERR_WRONG_AMOUNT_FORMAT = "ERR - WRONG AMOUNT FORMAT.";
    public static final String OK = "OK";
    public static final String UNKNOWN_COMMAND = "UNKNOWN COMMAND";
    private final RabbitTemplate rabbitTemplate;

    private final Pattern balancePattern = Pattern.compile(SmsMessageType.BALANCE.messageType());
    private final Pattern sendPattern = Pattern.compile(SmsMessageType.SEND.messageType());
    private final Pattern totalToUserPattern = Pattern.compile(SmsMessageType.TOTAL_TO_USER.messageType());
    private final Pattern totalBetweenUsersPattern = Pattern.compile(SmsMessageType.TOTAL_BETWEEN_USERS.messageType());

    @Autowired
    public MessageProcessingServiceDefaultImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public String processIncomingMessage(String smsContent, String senderDeviceId) {
        log.info("Start of message processing.");
        String result = UNKNOWN_COMMAND;
        if (balancePattern.matcher(smsContent).matches()) {
            log.info("This is {} message.", SmsMessageType.BALANCE);
            return processBalanceMessage(senderDeviceId);
        } else if (sendPattern.matcher(smsContent).matches()) {
            log.info("This is {} message.", SmsMessageType.SEND);
            return processSendMessage(smsContent, senderDeviceId);
        } else if (totalToUserPattern.matcher(smsContent).matches()) {
            log.info("This is {} message.", SmsMessageType.TOTAL_TO_USER);

        } else if (totalBetweenUsersPattern.matcher(smsContent).matches()) {
            log.info("This is {} message.", SmsMessageType.TOTAL_BETWEEN_USERS);
        }
        log.error("Command not recognized!");
        return result;
    }

    private String processSendMessage(String smsContent, String senderDeviceId) {
        log.info("Processing send message {} from {}", smsContent, senderDeviceId);
        String result;
        SendEvent sendEvent = null;
        try {
            sendEvent = createSendEvent(smsContent, senderDeviceId);
        } catch (MessageFormatException e) {
            result = ERROR_WRONG_MESSAGE_FORMAT;
            return  result;
        } catch (AmountFormatExeption amountFormatExeption) {
            result = ERR_WRONG_AMOUNT_FORMAT;
            return  result;
        }
        log.info("Sending {} message to {} queue.", sendEvent, Queues.SEND_QUEUE);
        SendEventResponse sendEventResponse = (SendEventResponse)rabbitTemplate.convertSendAndReceive(Queues.SEND_QUEUE.name(), sendEvent);
        log.info("Received response for send event: {}", sendEventResponse);
        ResultMetaInfo resultMetaInfo = sendEventResponse.getMetaInfo();
        result = analyzeSendMessageProcessingResult(resultMetaInfo);
        return result;
    }

    private String analyzeSendMessageProcessingResult(ResultMetaInfo resultMetaInfo) {
        String result;
        log.info("Analyzing response meta info {} for send event response.", resultMetaInfo);
        if (resultMetaInfo.isSuccess()) {
            log.info("Send event was successfully processed.");
            result = OK;
        } else {
            log.error("Something went wrong.");
            EventProcessingError eventProcessingError = resultMetaInfo.getEventProcessingError();
            log.error("Received {} error status from send event processor.", eventProcessingError);
            if (eventProcessingError == EventProcessingError.INSUFFICIENT_FUNDS) {
                result = ERR_INSUFFICIENT_FUNDS;
            } else if (eventProcessingError == EventProcessingError.NO_USER) {
                result = ERR_NO_USER;
            } else {
                result = PLEASE_TRY_AGAIN_LATER;
            }
        }
        return result;
    }

    private BigDecimal parseAmountFromString(String amountAsString) throws ParseException {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        String pattern = "#";
        DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
        decimalFormat.setParseBigDecimal(true);
        BigDecimal amount = (BigDecimal)decimalFormat.parse(amountAsString);
        return amount;
    }

    private String processBalanceMessage(String senderDeviceId) {
        String result;
        BalanceEvent balanceEvent = new BalanceEvent(senderDeviceId);
        log.info("Sending balance event to {} queue.", SmsMessageType.BALANCE);
        BalanceEventResponse balanceResponse = (BalanceEventResponse)rabbitTemplate.convertSendAndReceive(Queues.BALANCE_QUEUE.name(), balanceEvent);
        ResultMetaInfo resultMetaInfo = balanceResponse.getMetaInfo();
        log.info("Received balance event response. Processing status is {}", resultMetaInfo);
        if (resultMetaInfo.isSuccess()) {
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(0);
            df.setMinimumFractionDigits(0);
            df.setGroupingUsed(false);
            BigDecimal amount = balanceResponse.getAmount();
            result = df.format(amount);
        } else {
            result = resultMetaInfo.getMessage();
        }
        return result;
    }

    private SendEvent createSendEvent(String smsContent, String senderDeviceId) throws MessageFormatException, AmountFormatExeption {
        log.info("Creating send event for message {} from {}", smsContent, senderDeviceId);
        Matcher sendPatternMatcher = sendPattern.matcher(smsContent);
        Pattern pattern = Pattern.compile(SmsMessageType.SEND.messageType());
        Matcher matcher = sendPattern.matcher(smsContent);
        String amountAsString;
        String targetUser;
        if (!matcher.find()) {
            log.error("Message {} has wrong format!", smsContent);
            throw new MessageFormatException();
        }
        amountAsString = matcher.group(2);
        targetUser = matcher.group(3);
        BigDecimal amount;
        try {
            amount = parseAmountFromString(amountAsString);
        } catch (ParseException e) {
            log.error("Amount in message {} has wrong format!", smsContent);
            throw new AmountFormatExeption();
        }
        SendEvent sendEvent = new SendEvent(amount, senderDeviceId, targetUser);
        log.info("Successfully created send message {}.", sendEvent);
        return sendEvent;
    }

}
