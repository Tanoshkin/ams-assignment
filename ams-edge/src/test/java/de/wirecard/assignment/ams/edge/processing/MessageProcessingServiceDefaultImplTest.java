package de.wirecard.assignment.ams.edge.processing;

import de.wirecard.assignment.ams.model.Queues;
import de.wirecard.assignment.ams.model.events.EventProcessingError;
import de.wirecard.assignment.ams.model.events.ResultMetaInfo;
import de.wirecard.assignment.ams.model.events.SendEvent;
import de.wirecard.assignment.ams.model.events.SendEventResponse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.math.BigDecimal;

import static de.wirecard.assignment.ams.edge.processing.MessageProcessingServiceDefaultImpl.*;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by a.tanoshkin on 10.04.2017.
 */
public class MessageProcessingServiceDefaultImplTest {

    private static MessageProcessingServiceDefaultImpl messageProcessingService;

    @BeforeClass
    public static void setUp() throws Exception {
        RabbitTemplate mockRabbitTemplate = mock(RabbitTemplate.class);
        configureMockRabbitForGoodSendEvent(mockRabbitTemplate);
        configureMockRabbitForInsufficientFoundsSendEvent(mockRabbitTemplate);
        configureMockRabbitForUserDoesNotExistSendEvent(mockRabbitTemplate);
        messageProcessingService = new MessageProcessingServiceDefaultImpl(mockRabbitTemplate);
    }

    private static void configureMockRabbitForGoodSendEvent(RabbitTemplate mockRabbitTemplate) {
        SendEvent goodSendEvent = new SendEvent(BigDecimal.valueOf(100), "123", "FFRITZ");
        ResultMetaInfo goodSendEventMeta = new ResultMetaInfo(true, "1500", EventProcessingError.NO_ERROR);
        SendEventResponse goodSendEventResponse = new SendEventResponse();
        goodSendEventResponse.setMetaInfo(goodSendEventMeta);
        when(mockRabbitTemplate.convertSendAndReceive(Queues.SEND_QUEUE.name(), goodSendEvent)).thenReturn(goodSendEventResponse);
    }

    private static void configureMockRabbitForInsufficientFoundsSendEvent(RabbitTemplate mockRabbitTemplate) {
        SendEvent insufficientFoundsSendEvent = new SendEvent(BigDecimal.valueOf(1_000_000), "123", "FFRITZ");
        ResultMetaInfo insufficientFoundsSendEventMeta = new ResultMetaInfo(false, "Insufficient founds for this transaction!", EventProcessingError.INSUFFICIENT_FUNDS);
        SendEventResponse insufficientFoundsSendEventResponse = new SendEventResponse();
        insufficientFoundsSendEventResponse.setMetaInfo(insufficientFoundsSendEventMeta);
        when(mockRabbitTemplate.convertSendAndReceive(Queues.SEND_QUEUE.name(), insufficientFoundsSendEvent)).thenReturn(insufficientFoundsSendEventResponse);
    }

    private static void configureMockRabbitForUserDoesNotExistSendEvent(RabbitTemplate mockRabbitTemplate) {
        SendEvent userDoesNotExistSendEvent = new SendEvent(BigDecimal.valueOf(100), "123", "JDOE");
        ResultMetaInfo userDoesNotExistSendEventMeta = new ResultMetaInfo(false, "Target user does not exist!", EventProcessingError.NO_USER);
        SendEventResponse userDoesNotExistSendEventResponse = new SendEventResponse();
        userDoesNotExistSendEventResponse.setMetaInfo(userDoesNotExistSendEventMeta);
        when(mockRabbitTemplate.convertSendAndReceive(Queues.SEND_QUEUE.name(), userDoesNotExistSendEvent)).thenReturn(userDoesNotExistSendEventResponse);

    }

    @Test
    public void whenWeSendCorrectSendMessageWeReceiveCorrectResult() {
        String correctSmsMessage = "SEND-100-FFRITZ";
        String senderDeviceId = "123";
        String processingResult = messageProcessingService.processIncomingMessage(correctSmsMessage, senderDeviceId);
        assertTrue(processingResult.equals(OK));
    }

    @Test
    public void whenWeSendUnknownMessageFormatWeReceiveCorrespondingResult() {
        String incorrectSmsMessage = "SENT-100-FFRITZ";
        String senderDeviceId = "123";
        String processingResult = messageProcessingService.processIncomingMessage(incorrectSmsMessage, senderDeviceId);
        assertTrue(processingResult.equals(UNKNOWN_COMMAND));
    }

    @Test
    public void whenWeSendCorrectMessageButUserDoesntHaveEnoughMoneyWeReceiveCorrespondingResult() {
        String insufficientFoundsSmsMessage = "SEND-1000000-FFRITZ";
        String senderDeviceId = "123";
        String processingResult = messageProcessingService.processIncomingMessage(insufficientFoundsSmsMessage, senderDeviceId);
        assertTrue(processingResult.equals(ERR_INSUFFICIENT_FUNDS));
    }

    @Test
    public void whenWeSendCorrectMessageButUserDoesntExistWeReceiveCorrespondingResult() {
        String userDoesntExistSmsMessage = "SEND-100-JDOE";
        String senderDeviceId = "123";
        String processingResult = messageProcessingService.processIncomingMessage(userDoesntExistSmsMessage, senderDeviceId);
        assertTrue(processingResult.equals(ERR_NO_USER));
    }
}