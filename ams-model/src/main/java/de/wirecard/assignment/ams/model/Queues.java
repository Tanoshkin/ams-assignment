package de.wirecard.assignment.ams.model;

/**
 * Created by a.tanoshkin on 13.04.2017.
 */
public enum Queues {
    AMS_EXCHANGE,
    BALANCE_QUEUE,
    SEND_QUEUE,
    TRN_HISTORY_QUEUE
}
