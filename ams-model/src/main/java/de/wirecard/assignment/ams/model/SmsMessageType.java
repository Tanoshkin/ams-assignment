package de.wirecard.assignment.ams.model;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
public enum SmsMessageType {

    /*
    public static final String TOTAL_REGEX_BETWEEN_USERS = "TOTAL-(\\w+)-(\\w+)-(\\w+)";
    */

    BALANCE("BALANCE"),
    SEND("(SEND)-(\\d+)-(\\w+)"),
    TOTAL_TO_USER("TOTAL-(\\w+)-(\\w+)"),
    TOTAL_BETWEEN_USERS("TOTAL-(\\w+)-(\\w+)-(\\w+)"),
    UNKNOWN("");

    private String matchingRegex;

    SmsMessageType(String matchingRegex) {
        this.matchingRegex = matchingRegex;
    }

    public String messageType() {
        return matchingRegex;
    }

}
