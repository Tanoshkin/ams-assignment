package de.wirecard.assignment.ams.model.events;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
@Data
@AllArgsConstructor
public class BalanceEvent {
    String deviceId;
}
