package de.wirecard.assignment.ams.model.events;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
@Data
public class BalanceEventResponse {
    BigDecimal amount;
    ResultMetaInfo metaInfo;
}
