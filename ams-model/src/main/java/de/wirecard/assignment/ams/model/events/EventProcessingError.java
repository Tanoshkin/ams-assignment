package de.wirecard.assignment.ams.model.events;

/**
 * Created by a.tanoshkin on 16.04.2017.
 */
public enum EventProcessingError {
    INSUFFICIENT_FUNDS,
    NO_USER,
    INTERNAL_ERROR,
    NO_ERROR
}
