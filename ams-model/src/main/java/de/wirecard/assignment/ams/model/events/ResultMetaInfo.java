package de.wirecard.assignment.ams.model.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultMetaInfo {
    boolean success;
    String message;
    EventProcessingError eventProcessingError;
}
