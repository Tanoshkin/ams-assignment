package de.wirecard.assignment.ams.model.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.math.BigDecimal;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
@Data
@AllArgsConstructor
public class SendEvent {
    BigDecimal amount;
    String deviceId;
    String targetUser;
}
