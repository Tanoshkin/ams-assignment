package de.wirecard.assignment.ams.model.events;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
@Data
public class TotalSentEvent {
    BigDecimal amount;
    String deviceId;
    List<String> targetUserList;
}
