package de.wirecard.assignment.ams.model.events;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
public class TotalSentEventResponse {
    Map<String, BigDecimal> amountsSentToTargetUsers;
    ResultMetaInfo metaInfo;
}
