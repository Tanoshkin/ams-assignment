package de.wirecard.assignment.ams.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * Created by a.tanoshkin on 14.04.2017.
 */
@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
public class AmsServiceApp {

    public static void main(String[] args) {
        SpringApplication.run(AmsServiceApp.class, args);
    }

}
