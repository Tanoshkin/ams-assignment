package de.wirecard.assignment.ams.processor.adp;

import de.wirecard.assignment.ams.api.TransferManager;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by a.tanoshkin on 16.04.2017.
 */
@FeignClient("http://backend-mocks")
@Component
public interface TransferManagerClient extends TransferManager {

    @Override
    @RequestMapping(value = "/send_money", method = RequestMethod.PATCH)
    void sendMoney(@RequestParam String senderUsername, String recipientUsername, BigDecimal amount);

    @Override
    List<BigDecimal> getAllTransactions(String senderUsername, String recipientUsername);
}
