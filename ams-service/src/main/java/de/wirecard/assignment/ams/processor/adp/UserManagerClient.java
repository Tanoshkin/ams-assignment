package de.wirecard.assignment.ams.processor.adp;

import de.wirecard.assignment.ams.api.UserManager;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * Created by a.tanoshkin on 14.04.2017.
 */
@FeignClient("http://backend-mocks")
@Component
public interface UserManagerClient extends UserManager {

    @Override
    @RequestMapping(value = "/user-mgmt/balance", method = RequestMethod.GET)
    BigDecimal getBalance(@RequestParam(name = "username") String username);

    @Override
    @RequestMapping(value = "/user-mgmt/exist", method = RequestMethod.GET)
    public boolean existsUser(@RequestParam(name = "username") String username);

    @Override
    @RequestMapping(value = "/user-mgmt/user", method = RequestMethod.GET)
    public String getUserNameForDeviceId(@RequestParam(name = "deviceId") String deviceId);

}
