package de.wirecard.assignment.ams.processor.balance;

import de.wirecard.assignment.ams.model.events.BalanceEvent;
import de.wirecard.assignment.ams.model.events.BalanceEventResponse;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
public interface BalanceEventProcessor {
    public BalanceEventResponse newBalanceEvent(BalanceEvent balanceEvent);
}
