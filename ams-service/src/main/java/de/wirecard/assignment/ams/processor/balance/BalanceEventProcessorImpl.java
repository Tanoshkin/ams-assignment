package de.wirecard.assignment.ams.processor.balance;

import de.wirecard.assignment.ams.model.events.BalanceEvent;
import de.wirecard.assignment.ams.model.events.BalanceEventResponse;
import de.wirecard.assignment.ams.model.events.ResultMetaInfo;
import de.wirecard.assignment.ams.processor.adp.UserManagerClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Created by a.tanoshkin on 11.04.2017.
 */
@Component
@Slf4j
public class BalanceEventProcessorImpl implements BalanceEventProcessor {

    private final UserManagerClient userManagerClient;

    @Autowired
    public BalanceEventProcessorImpl(UserManagerClient userManagerClient) {
        this.userManagerClient = userManagerClient;
    }

    @Override
    public BalanceEventResponse newBalanceEvent(BalanceEvent balanceEvent) {
        String deviceId = balanceEvent.getDeviceId();
        log.info("Balance event received for device id: {}.", deviceId);
        BalanceEventResponse response = new BalanceEventResponse();
        ResultMetaInfo metaInfo = new ResultMetaInfo();
        try {
            log.info("Trying to find user by device id.");
            String userName = userManagerClient.getUserNameForDeviceId(deviceId);
            if (!userName.isEmpty()) {
                log.info("Found user {}. Trying to query balance.", userName);
                BigDecimal balance = userManagerClient.getBalance(userName);
                log.info("Balance received.");
                metaInfo.setSuccess(true);
                response.setAmount(balance);
            } else {
                log.error("User with device id {} not found.", deviceId);
                metaInfo.setSuccess(false);
                metaInfo.setMessage("ERR - NO USER.");
            }
        } catch (Exception e) {
            log.error("Exception {} occurred while processing balance event for device id {}.", e.getMessage(), deviceId);
            metaInfo.setSuccess(false);
            metaInfo.setMessage("Internal error during processing.");
        }
        response.setMetaInfo(metaInfo);
        return response;
    }
}
