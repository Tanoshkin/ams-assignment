package de.wirecard.assignment.ams.processor.config;

import de.wirecard.assignment.ams.model.Queues;
import de.wirecard.assignment.ams.processor.balance.BalanceEventProcessor;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by a.tanoshkin on 14.04.2017.
 */
@Configuration
public class MessagingConfig {

    @Bean
    MessageListenerAdapter balanceListener(BalanceEventProcessor balanceEventProcessor, MessageConverter converter) {
        MessageListenerAdapter newBalanceEvent = new MessageListenerAdapter(balanceEventProcessor, "newSendEvent");
        newBalanceEvent.setMessageConverter(converter);
        return newBalanceEvent;
    }

    @Bean
    SimpleMessageListenerContainer balanceContainer(ConnectionFactory connectionFactory,
                                                    MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(Queues.BALANCE_QUEUE.name());
        container.setMessageListener(listenerAdapter);
        container.setAcknowledgeMode(AcknowledgeMode.AUTO);
        container.setConcurrentConsumers(20);
        return container;
    }


    @Bean
    public MessageConverter jsonMessageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(Queues.AMS_EXCHANGE.name());
    }

    @Bean
    Queue balanceQueue() {
        return new Queue(Queues.BALANCE_QUEUE.name(), false);
    }

    @Bean
    Queue sendQueue() {
        return new Queue(Queues.SEND_QUEUE.name(), false);
    }

    @Bean
    Queue trnHistQueue() {
        return new Queue(Queues.TRN_HISTORY_QUEUE.name(), false);
    }

    @Bean
    Binding balanceBinding() {
        return BindingBuilder.bind(balanceQueue()).to(exchange()).with(Queues.BALANCE_QUEUE.name());
    }

    @Bean
    Binding sendBinding() {
        return BindingBuilder.bind(sendQueue()).to(exchange()).with(Queues.SEND_QUEUE.name());
    }

    @Bean
    Binding trnHistBinding() {
        return BindingBuilder.bind(trnHistQueue()).to(exchange()).with(Queues.TRN_HISTORY_QUEUE.name());
    }


}
