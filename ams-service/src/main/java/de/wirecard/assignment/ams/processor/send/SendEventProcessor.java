package de.wirecard.assignment.ams.processor.send;

import de.wirecard.assignment.ams.model.events.BalanceEvent;
import de.wirecard.assignment.ams.model.events.BalanceEventResponse;
import de.wirecard.assignment.ams.model.events.SendEvent;
import de.wirecard.assignment.ams.model.events.SendEventResponse;

/**
 * Created by a.tanoshkin on 16.04.2017.
 */
public interface SendEventProcessor {
    public SendEventResponse newSendEvent(SendEvent sendEvent);
}
