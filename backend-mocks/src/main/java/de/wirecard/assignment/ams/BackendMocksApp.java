package de.wirecard.assignment.ams;

import de.wirecard.assignment.ams.mock.backend.UserEntity;
import de.wirecard.assignment.ams.mock.backend.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.stream.Stream;

/**
 * Created by a.tanoshkin on 12.04.2017.
 */
@SpringBootApplication
@Slf4j
public class BackendMocksApp {

    public static void main(String[] args) {
        SpringApplication.run(BackendMocksApp.class, args);
    }


    @Service
    public class BackendInitializer implements CommandLineRunner {

        private final UserRepository repository;

        @Autowired
        public BackendInitializer(UserRepository repository) {
            this.repository = repository;
        }

        @Override
        public void run(String... args) throws Exception {
            log.info("Initializing mock database.");
            Stream.of(
                    new UserEntity(Long.valueOf(1), "FFRITZ", "123", BigDecimal.valueOf(1500)),
                    new UserEntity(Long.valueOf(2), "MSMITH", "321", BigDecimal.valueOf(500)),
                    new UserEntity(Long.valueOf(3), "PIVANOV", "213", BigDecimal.valueOf(100)))
                    .forEach(u -> {
                        log.info("Storing user {} to mock database.",u.getUsername());
                        repository.save(u);
                    }
            );
            log.info("Initialization completed.");
        }
    }
}
