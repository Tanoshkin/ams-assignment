package de.wirecard.assignment.ams.mock.backend;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by a.tanoshkin on 12.04.2017.
 */
@Data
@AllArgsConstructor
@Entity
public class Transaction {

    public Transaction() {
        // WHY JPA, WHY?!
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;

    @ManyToOne(targetEntity = UserEntity.class)
    @JoinColumn(name="sender_id", referencedColumnName = "id")
    UserEntity sender;

    @ManyToOne(targetEntity = UserEntity.class)
    @JoinColumn(name="receiver_id", referencedColumnName = "id")
    UserEntity receiver;

    BigDecimal amount;


}
