package de.wirecard.assignment.ams.mock.backend;

import org.h2.mvstore.db.TransactionStore;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by a.tanoshkin on 12.04.2017.
 */
public interface TransactionRepository extends CrudRepository<Transaction, Long> {
    List<Transaction> findByReceiver(UserEntity recipient);
    List<Transaction> findBySenderAndReceiver(UserEntity sender, UserEntity recipient);
}
