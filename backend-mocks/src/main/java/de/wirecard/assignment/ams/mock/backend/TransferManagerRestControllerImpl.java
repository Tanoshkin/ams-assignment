package de.wirecard.assignment.ams.mock.backend;

import de.wirecard.assignment.ams.api.TransferManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by a.tanoshkin on 12.04.2017.
 */
@RestController
@RequestMapping(path = "/user_mgmt")
@Slf4j
@EnableEurekaClient
public class TransferManagerRestControllerImpl implements TransferManager {

    private final UserRepository userRepository;
    private final TransactionRepository transactionRepository;

    @Autowired
    public TransferManagerRestControllerImpl(UserRepository userRepository, TransactionRepository transactionRepository) {
        this.userRepository = userRepository;
        this.transactionRepository = transactionRepository;
    }

    @Override
    @RequestMapping(value = "/send_money", method = RequestMethod.PATCH)
    @Transactional
    public void sendMoney(@RequestParam String senderUsername, @RequestParam String recipientUsername, @RequestParam BigDecimal amount) {
        log.info("Sending money ({} EUR) from {} to {}",amount.toString(),senderUsername, recipientUsername);
        Optional<UserEntity> oSender = userRepository.getByUsername(senderUsername);
        Optional<UserEntity> oReceiver = userRepository.getByUsername(recipientUsername);
        UserEntity sender = oSender.get();
        UserEntity receiver = oReceiver.get();
        Transaction transaction = new Transaction(Long.valueOf(1), sender, receiver, amount);
        transactionRepository.save(transaction);
        sender.setBalance(sender.getBalance().subtract(amount));
        log.info("Decreased sender balance.");
        receiver.setBalance(receiver.getBalance().add(amount));
        log.info("Increased recipient balance.");
    }

    @Override
    @RequestMapping(value = "/transactions", method = RequestMethod.GET)
    public List<BigDecimal> getAllTransactions(@RequestParam String senderUsername, @RequestParam String recipientUsername) {
        List<BigDecimal> results = new ArrayList<>();
        List<Transaction> transactions = new ArrayList<>();
        Optional<UserEntity> oSender = userRepository.getByUsername(senderUsername);
        if (!recipientUsername.isEmpty()) {
            Optional<UserEntity> oReceiver;
            oReceiver = userRepository.getByUsername(recipientUsername);
            if (!senderUsername.isEmpty()) {
                oSender = userRepository.getByUsername(senderUsername);
                List<Transaction> transactionsBetweenUsers = transactionRepository.findBySenderAndReceiver(oSender.get(),oReceiver.get());
                return transactionsBetweenUsers.stream().map(t -> t.amount).collect(Collectors.toList());
            }
            List<Transaction> transactionsToOneUser = transactionRepository.findByReceiver(oReceiver.get());
            return transactionsToOneUser.stream().map(t -> t.amount).collect(Collectors.toList());
        } else {
            return results;
        }
    }



}
