package de.wirecard.assignment.ams.mock.backend;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by a.tanoshkin on 12.04.2017.
 */
@Data
@AllArgsConstructor
@Entity
public class UserEntity {

    public UserEntity() {
        // WHY JPA, WHY?!
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String username;
    private String deviceId;
    private BigDecimal balance;
}
