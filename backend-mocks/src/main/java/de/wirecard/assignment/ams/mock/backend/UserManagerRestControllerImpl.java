package de.wirecard.assignment.ams.mock.backend;


import de.wirecard.assignment.ams.api.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Created by a.tanoshkin on 12.04.2017.
 */
@RestController
@RequestMapping(path = "/user-mgmt")
public class UserManagerRestControllerImpl implements UserManager {

    private final UserRepository userRepository;

    /* This data structure just stores users instead of some real DB.
     */
    private final UserEntity nullUser = new UserEntity(null,null,null,null);

    @Autowired
    public UserManagerRestControllerImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @RequestMapping(value = "/exist", method = RequestMethod.GET)
    public boolean existsUser(@RequestParam String username) {
        Optional<UserEntity> user = userRepository.getByUsername(username);
        return user.isPresent();
    }

    @Override
    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public BigDecimal getBalance(@RequestParam String username) {
        Optional<UserEntity> user = userRepository.getByUsername(username);
        if (user.isPresent()) {
            return user.get().getBalance();
        } else {
            return null;
        }
    }

    @Override
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String getUserNameForDeviceId(@RequestParam String deviceId) {
        Optional<UserEntity> user = userRepository.getByDeviceId(deviceId);
        if (user.isPresent()) {
            return user.get().getUsername();
        } else {
            return null;
        }
    }


}
