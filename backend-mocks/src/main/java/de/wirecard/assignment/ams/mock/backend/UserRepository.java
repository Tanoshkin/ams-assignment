package de.wirecard.assignment.ams.mock.backend;

import de.wirecard.assignment.ams.mock.backend.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.AssertTrue;
import java.util.Optional;

/**
 * Created by a.tanoshkin on 12.04.2017.
 */
public interface UserRepository extends CrudRepository<UserEntity, Long> {
    @Transactional
    Optional<UserEntity> getByUsername(String username);
    @Transactional
    Optional<UserEntity> getByDeviceId(String deviceId);
}
